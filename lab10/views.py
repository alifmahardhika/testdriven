from django.shortcuts import render,redirect
from django.http import JsonResponse
from django.http import HttpResponse
from .forms import FormSubscriber
from .models import Subscriber
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def subscribe(request):
    form = FormSubscriber(request.POST)
    if request.method == 'POST' and form.is_valid():
        data = form.cleaned_data
        status_subscribe = True
        try:
            Subscriber.objects.create(**data)
        except:
            status_subscribe = False
        return JsonResponse({'status_subscribe' : status_subscribe})
    content = {'form': form}
    return render(request, 'subscribe.html', content)

@csrf_exempt
def checkEmail(request):
    if request.method == "POST":
        email = request.POST['email']
        is_email_already_exist = Subscriber.objects.filter(pk=email).exists()
        return JsonResponse({'is_email': is_email_already_exist})

def listSubscriber(request):
    all_subs = Subscriber.objects.all().values()
    subs = list(all_subs)
    return JsonResponse({'all_subs' : subs})

@csrf_exempt
def unsubscribe(request):
    if request.method == 'POST':
        email = request.POST['email']
        Subscriber.objects.get(email=email).delete()
        return redirect('subscribe')

# def subsList():
#     subs = Subscriber.objects.all()
