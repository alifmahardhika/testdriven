from django.shortcuts import render, redirect

# Create your views here.
def load_page(request):
    response = {}
    return render(request, 'home.html', response)
