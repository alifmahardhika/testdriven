from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import create_form
from .models import Kabar
from .forms import FormKabar
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest


# Create your tests here.
class Lab6_Test(TestCase):
    def test_lab_6_url_is_exist(self):
        response = Client().get('/lab6/')
        self.assertEqual(response.status_code, 200)

    def test_model_can_create_new_status(self):
        new_mood = Kabar.objects.create(mood='halo semua')
        allmood = Kabar.objects.all().count()
        self.assertEqual(allmood,1)

    def test_can_save_a_POST_request(self):
        Kabar.objects.create(mood='santai')
        countmood = Kabar.objects.all().count()
        self.assertEqual(countmood,1)
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')


    def test_lab6_using_to_do_list_template(self):
        response = Client().get('/lab6/')
        self.assertTemplateUsed(response, 'howru.html')

    def test_lab6_using_create_form_func(self):
        found= resolve('/lab6/')
        self.assertEqual(found.func, create_form)

#selenium
# class StatusTestCase(LiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('disable-gpu')
#         chrome_options.add_argument('--disable-dev-shm-usage')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25)
#         super(StatusTestCase, self).setUp()
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(StatusTestCase, self).tearDown()
#
#     def test_status(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url)
#         kabar = selenium.find_element_by_id('id_mood')
#         kabar.send_keys('capeeeee')
#         self.selenium.implicitly_wait(5)
#         submit = selenium.find_element_by_id('submitbutton')
#         submit.send_keys(Keys.RETURN)
#         self.assertIn('capeeeee', selenium.page_source)
#
#     def test_position_home(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url)
#         kabar = selenium.find_element_by_id('id_mood')
#         size_kabar = kabar.size
#         location_kabar = kabar.location
#         # self.assertEqual(size_kabar,{'height': 49, 'width': 275})
#         #self.assertEqual(location_kabar, {'x': 457, 'y': 276})
#
#     def test_position_howru(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url)
#         kabar = selenium.find_element_by_id('howru')
#         size_kabar = kabar.size
#         location_kabar = kabar.location
#         #self.assertEqual(size_kabar,{'height': 56, 'width': 288})
#         #self.assertEqual(location_kabar, {'x': 40, 'y': 272})
#
#     def test_css_home(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url)
#         home = selenium.find_element_by_id('home')
#         warna_home = home.value_of_css_property('background-image')
#         self.assertEqual(warna_home, "linear-gradient(to right, white, lightblue)")
#
#     def test_css_kabar(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url)
#         kabar = selenium.find_element_by_id('recorded')
#         warna = kabar.value_of_css_property('color')
#         size = kabar.value_of_css_property('font-size')
#         family = kabar.value_of_css_property('font-family')
#         self.assertEqual(warna, 'rgba(0, 0, 0, 1)')
#         #self.assertEqual(size, '30px')
#         self.assertEqual(family, 'Arial')
