from django.shortcuts import render
from .forms import FormKabar
from .models import Kabar

# Create your views here.
def create_form(request):
    form = FormKabar(request.POST or None)
    response = {}
    if(request.method == "POST"):
        kabar2 = Kabar.objects.all()
        response = {
            "kabar2" : kabar2
        }
        response['form'] = form
        if (form.is_valid()):
            mood = request.POST.get("mood")
            Kabar.objects.create(mood=mood)
            return render(request, 'howru.html', response)
        else:
            return render(request, 'howru.html', response)
    else:
        kabar2 = Kabar.objects.all()
        response = {
            "kabar2" : kabar2
        }
        response['form'] = form
        return render(request, 'howru.html', response)