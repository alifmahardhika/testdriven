from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def data(request, text):
	json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + text).json()
	# json_parse = json.dumps(json_read)
	return JsonResponse(json_read)

def load_page(request):
    response = {}
    return render(request, 'story9.html')
