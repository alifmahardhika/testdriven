// $(document).ready(function() {
// 	$.ajax({
// 		url: "data",
// 		datatype: 'json',
// 		success: function(data){
// 			var obj = jQuery.parseJSON(data)
// 			renderHTML(obj);
// 		}
// 	})
// });

var container = document.getElementById("demo");
function renderHTML(data){
	htmlstring = "<tbody>";
	for(i = 0; i < data.items.length;i++){
		htmlstring += "<tr>"+
		"<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
		"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
		"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
		"<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
		"<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" +
		"<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" +
		"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/png/512/660/660463.png'>" + "</td></tr>";
	}
	container.insertAdjacentHTML('beforeend', htmlstring+"</tbody>")
}

var counter = 0;
function favorite(clicked_id){
	var btn = document.getElementById(clicked_id);
	if(btn.classList.contains("checked")){
		btn.classList.remove("checked");
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/png/512/660/660463.png';
		counter--;
		var count = document.getElementById("counter").innerHTML = counter;
	}
	else{
		btn.classList.add('checked');
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
		counter++;
		var count = document.getElementById("counter").innerHTML = counter;
	}
}

function quilting(){
	$(document).ready(function() {
	$.ajax({
		url: "data/quilting",
		datatype: 'json',
		success: function(data){
			$('tr').remove()
			var result ='<tr>';
			for(var i = 0; i < data.items.length; i++) {
				result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" +
				"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
				"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/png/512/660/660463.png'>" + "</td></tr>";
			}
			$('tbody').append(result);
		}
	})
});
}

function tweeting(){
	$(document).ready(function() {
		$.ajax({
			url: "data/tweeting",
			datatype: 'json',
			success: function(data){
				$('tr').remove()
				var result ='<tr>';
				for(var i = 0; i < data.items.length; i++) {
					result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
					"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
					"<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
					"<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" +
					"<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" +
					"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
					"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/png/512/660/660463.png'>" + "</td></tr>";
				}
				$('tbody').append(result);
			}
		})
	});
}

function quitting(){
	$(document).ready(function() {
	$.ajax({
		url: "data/quitting",
		datatype: 'json',
		success: function(data){
			$('tr').remove()
			var result ='<tr>';
			for(var i = 0; i < data.items.length; i++) {
				result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" +
				"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
				"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/png/512/660/660463.png'>" + "</td></tr>";
			}
			$('tbody').append(result);
		}
	})
});
}
