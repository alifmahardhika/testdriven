$(document).ready(function () {
    $('#submit_subscribe').prop('disabled', true);
    $('.unsubBerhasil').hide();

    $('#form_name').hide();
    $('#form_email').hide();
    $('#form_email2').hide();
    $('#form_password').hide();
    $('#name').val("");
    $('#email').val("");
    $('#password').val("");

    var flag = [false, false, false, false] //set flag untuk tiap field yg perlu validasi sebagai false dulu

    $('#name').on('input', function () {
        var input = $(this); //ambil nama
        check(input, 0); //panggil method check, angka 0 untuk speifikasi pengecekan
        checkButton();
    });

    var timer = null;
    $('#email').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(function () {
            var input = $("#email"); //ambil email
            check(input, 1);//panggil method check, angka 0 untuk speifikasi pengecekan
            checkButton(); //set button submit jadi tidak disabled, tapi kalo ttp masih ada flag alert ttp tidak bisa
        }, 1000)
    });

    $('#password').on('input', function () {
        var input = $(this); //ambil password
        check(input, 2); //panggil method check, angka 0 untuk speifikasi pengecekan
        checkButton();
    });

    var check = function (input, arr) {
        if (arr === 1) { //untuk spesifikasi pengecekan email
            var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/; //cek string email
            var is_data = reg.test(input.val()); // kalo bener maka true
            if (is_data) {
                $('#form_email').hide();
                checkEmail(input.val()); // ambil data formnya
                flag[arr] = true;   //kalo ada alert maka keluar deh alertnya
                return
            } else {
                $('#form_email2').hide();      //kalo gaada alert maka alertnya dihide
            }
        } else {
            var is_data = input.val();
        }
        if (is_data) {  //kalau field sudah diisi
            if (arr === 0) {    //spesifikasi pengecekan nama
                $('#form_name').hide();
            } else if (arr === 1) {
                $('#form_email').hide();
            } else {
                $('#form_password').hide();
            }
            flag[arr] = true;
        } else {    //kalau field masih kosong
            if (arr === 0) {
                $('#form_name').show();
            } else if (arr === 1) {
                $('#form_email').show();
            } else {
                $('#form_password').show();
            }
            flag[arr] = false;
        }
    };

    var checkEmail = function (email) {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "check-email/",
            headers: {
                "X-CSRFToken": csrftoken
            },
            data: {email: email},
            success: function (response) { // responsenya boolean yg ngecek apakah email sudah registered
                if (response.is_email) {
                    $('#form_email2').show();
                    flag[3] = false;
                    checkButton();
                } else {
                    $('#form_email2').hide();
                    flag[3] = true;
                    checkButton();
                }
            },
            error: function (error) {
                alert("Error, cannot get data from server")
            }
        })
    };

    var checkButton = function () {
        var button_subscribe = $('#submit_subscribe');
        for (var x = 0; x < flag.length; x++) {
            if (flag[x] === false) {
                button_subscribe.prop('disabled', true);
                return
            }
        }
        button_subscribe.prop('disabled', false);
    };

    $(function () {
        $('form').on('submit', function (e) {
            window.location.reload();
            // e.preventDefault(); //biar page tidak refresh
            $.ajax({
                method: "POST",
                url: '',
                data: $('form').serialize(),
                success: function (status) {
                    if (status) {
                        alert("Subscription succesful!");
                    } else {
                        alert("Something went wrong, please try again.");
                    }
                    for (var i = 0; i < flag.length; i++) {
                        flag[i] = false
                    }
                    $("#submit_subscribe").prop('disabled', true);
                    $('#name').val("");
                    $('#email').val("");
                    $('#password').val("");
                },
                error: function (error) {
                    alert("Error, cannot connect to server")
                }
            });
        });
    });

    $.ajax({
        url: "list-subscriber/",
        datatype: 'json',
        success: function(data){
            $('tbody').html('')
            var result ='<tr>';
            for(var i = 0; i < data.all_subs.length; i++) {
                result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
                "<td class='align-middle text-center'>" + data.all_subs[i].name +"</td>" +
                "<td class='align-middle text-center'>" + data.all_subs[i].email +"</td>" +
                "<td class='align-middle text-center'>" + "<a " + "data-email=" + data.all_subs[i].email + " class='text-white btn btn-danger unsubscribe-button' float-right role='button' aria-pressed='true'>" + "Unsubscribe" + "</a></td></tr>";
            }
            $('tbody').append(result);
        },
        error: function(error){
            alert("There's no any subcribers yet");
        }
    })

    $('#demo').on('click', 'td .unsubscribe-button', function() {
        var email = $(this).attr('data-email');
        unsubscribe(email);
    });
});

function unsubscribe(email){
    $.ajax({
        method: "POST",
        url: "unsubscribe/",
        data: {email: email},
        success: function(){
            $('.unsubBerhasil').show();
            window.location.reload();
        },
    })
}
